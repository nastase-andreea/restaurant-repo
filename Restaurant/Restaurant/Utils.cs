﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Restaurant
{
    public class Utils
    {
        public static int VerifyUser(string email, string parola)
        {
            LinkToSqlClassDataContext dc = new LinkToSqlClassDataContext(Properties.Settings.Default.RestaurantConnectionString);
            if(dc.DatabaseExists())
            {
                List<Client> clientList = dc.Clients.ToList<Client>();
                foreach(Client client in clientList)
                {
                    if (client.Email.Equals(email) && client.Parola.Equals(parola))
                        return 1;
                }

                List<Angajat> angajatList = dc.Angajats.ToList<Angajat>();
                foreach (Angajat angajat in angajatList)
                {
                    if (angajat.Email.Equals(email) && angajat.Parola.Equals(parola))
                        return 2;
                }
            }
            return 0;
        }

        public static int CautaIdClient(string email, string parola)
        {
            LinkToSqlClassDataContext dc = new LinkToSqlClassDataContext(Properties.Settings.Default.RestaurantConnectionString);
            if (dc.DatabaseExists())
            {
                foreach(Client c in dc.Clients)
                {
                    if (c.Email.Equals(email) && c.Parola.Equals(parola))
                        return c.IdClient;
                }
            }
            return 0;
        }

        public static bool CampGol(string nume, string prenume, string email, string parola, string telefon, string adresa)
        {
            if(((nume == string.Empty || prenume==string.Empty) || (email == string.Empty || parola == string.Empty)) || (telefon == string.Empty || adresa == string.Empty))
                return true;
            return false;
        }

        public static bool ClientExistent(string email)
        {
            LinkToSqlClassDataContext dc = new LinkToSqlClassDataContext(Properties.Settings.Default.RestaurantConnectionString);

            foreach (Client client in dc.Clients)
            {
                if (client.Email.Equals(email))
                    return true;
            }
            return false;
        }
        public static void AdaugaClient(string nume, string prenume, string email, string parola, string telefon, string adresa)
        {
            LinkToSqlClassDataContext dc = new LinkToSqlClassDataContext(Properties.Settings.Default.RestaurantConnectionString);
            int ultim = dc.Clients.ToList()[dc.Clients.ToList().Count-1].IdClient;
            Client client = new Client
            {
                Nume = nume,
                Prenume = prenume,
                Email = email,
                Telefon = telefon,
                AdresaLivrare = adresa,
                Parola = parola,
                IdClient = ultim + 1,
                Activ = true
            };

            dc.Clients.InsertOnSubmit(client);
            dc.SubmitChanges();
        }

        public static List<Preparat> GetPreparate(string categorie)
        {
            LinkToSqlClassDataContext dc = new LinkToSqlClassDataContext(Properties.Settings.Default.RestaurantConnectionString);
            List<Preparat> list = new List<Preparat>();
            int id = -1;
            foreach (PreparatTip p in dc.PreparatTips)
                if (p.Denumire.Equals(categorie))
                    id = p.IdPreparatTip;

            foreach (Preparat p in dc.Preparats)
                if (p.IdCategorie == id)
                    list.Add(p);

            return list;
        }

        public static List<Meniu> GetMeniuri(string categorie)
        {
            LinkToSqlClassDataContext dc = new LinkToSqlClassDataContext(Properties.Settings.Default.RestaurantConnectionString);
            List<Meniu> list = new List<Meniu>();

            foreach (Meniu p in dc.Menius)
                    list.Add(p);

            return list;
        }
        
        public static void AdaugaPreparat(List<MenuItem> list, Preparat p)
        {
            MenuItem m = new MenuItem();
            string image = FindImageUtils.FindPreparatImage(p.Denumire);
            string nume = p.Denumire + ", " + KgtoG(p.Cantitate) + "g, " + p.Pret.ToString() + "lei";
            m.ImageData = new BitmapImage(new Uri(image, UriKind.Relative));
            m.Description = nume;
            list.Add(m);
        }

        public static List<MenuItem> ArataPreparat(List<Preparat> preparate)
        {
            List<MenuItem> list = new List<MenuItem>();
            foreach (Preparat p in preparate)
            {
                AdaugaPreparat(list, p);
            }

            return list;
        }

        public static void AdaugaMeniu(List<MenuItem> list, Meniu p)
        {
            MenuItem m = new MenuItem();
            string image = FindImageUtils.FindMeniuImage(p.Denumire);
            string nume = p.Denumire + ", " + p.Pret.ToString() + "lei";
            m.ImageData = new BitmapImage(new Uri(image, UriKind.Relative));
            m.Description = nume;
            list.Add(m);
        }

        public static List<MenuItem> ArataMeniu(List<Meniu> meniuri)
        {
            List<MenuItem> list = new List<MenuItem>();
            foreach (Meniu p in meniuri)
            {
                AdaugaMeniu(list, p);
            }

            return list;
        }

        public static string KgtoG(double kgs)
        {
            return (kgs * 1000).ToString();
        }

        public static double GtoKg(int gs)
        {
            return ((double)gs / 1000);
        }

        public static List<MenuItem> Filtreaza(string filtru)
        {
            LinkToSqlClassDataContext dc = new LinkToSqlClassDataContext(Properties.Settings.Default.RestaurantConnectionString);
            List<MenuItem> list = new List<MenuItem>();
            
            foreach(Alergen a in dc.Alergens)
                if(a.Denumire.IndexOf(filtru, StringComparison.OrdinalIgnoreCase) != -1)
                {
                    GasesteDupaAlergen(dc, list, a.IdAlergen);
                    return list;
                }

            foreach(Preparat p in dc.Preparats)
                if(p.Denumire.IndexOf(filtru, StringComparison.OrdinalIgnoreCase) != -1)
                {
                    AdaugaPreparat(list, p);
                }

            foreach(Meniu p in dc.Menius)
                if (p.Denumire.IndexOf(filtru, StringComparison.OrdinalIgnoreCase) != -1)
                {
                    AdaugaMeniu(list, p);
                }

                return list;
        }

        public static void GasesteDupaAlergen(LinkToSqlClassDataContext dc, List<MenuItem> list, int idAlergen)
        {
            List<int> idPreparate = new List<int>();
            List<int> idMeniuri = new List<int>();
            foreach (PreparatAlergenJoin p in dc.PreparatAlergenJoins)
            {
                if (idPreparate.Contains(p.IdPreparat) == false && idAlergen == p.IdAlergen)
                    idPreparate.Add(p.IdPreparat);
            }

            foreach(MeniuPreparatJoin p in dc.MeniuPreparatJoins)
            {
                foreach(int idPrep in idPreparate)
                {
                    if (idMeniuri.Contains(p.IdMeniu) == false && idPrep == p.IdPreparat)
                        idMeniuri.Add(p.IdMeniu);
                }
            }

            foreach(Preparat p in dc.Preparats)
            {
                if(idPreparate.Contains(p.IdPreparat) == false)
                {
                    AdaugaPreparat(list, p);
                }
            }

            foreach(Meniu p in dc.Menius)
            {
                if(idMeniuri.Contains(p.IdMeniu) == false)
                {
                    AdaugaMeniu(list, p);
                }
            }
        }

        public static string GetDescription(MenuItem m)
        {
            return m.Description;
        }

        public static int CautaCantitatePreparat(string denumire)
        {
            LinkToSqlClassDataContext dc = new LinkToSqlClassDataContext(Properties.Settings.Default.RestaurantConnectionString);
            foreach (Preparat p in dc.Preparats)
                if (p.Denumire.Equals(denumire))
                {
                    return int.Parse(KgtoG(p.CantitateTotala));
                }
            return -1;
        }
        public static void CautaPreparateMeniu(string denumire, Dictionary<string, int> dict)
        {
            LinkToSqlClassDataContext dc = new LinkToSqlClassDataContext(Properties.Settings.Default.RestaurantConnectionString);
            foreach (Meniu p in dc.Menius)
                if (p.Denumire.Equals(denumire))
                {
                    foreach (MeniuPreparatJoin m in dc.MeniuPreparatJoins)
                    {
                        if (m.IdMeniu == p.IdMeniu)
                        {
                            foreach (Preparat pr in dc.Preparats)
                            {
                                if (pr.IdPreparat == m.IdPreparat)
                                {
                                    if (dict.ContainsKey(pr.Denumire))
                                        dict[pr.Denumire] += int.Parse(KgtoG(pr.Cantitate));
                                    else
                                        dict[pr.Denumire] = int.Parse(KgtoG(pr.Cantitate));
                                }
                                
                            }
                            
                        }
                    }
                    break;
                }

        }
        public static void ListeazaPreparateComanda(List<string> iteme_comanda, Dictionary<string, int> preparate)
        {
            foreach (string s in iteme_comanda)
            {
                List<string> item = s.Split(',').ToList();
                string denumire = item[0].TrimStart(' ');
                if (denumire.Contains("Meniu"))
                {
                    CautaPreparateMeniu(denumire, preparate);
                }
                else
                {
                    string cantitate = item[1].TrimStart(' ').TrimEnd('g');
                    if (preparate.ContainsKey(denumire))
                        preparate[denumire] += int.Parse(cantitate);
                    else
                        preparate[denumire] = int.Parse(cantitate);
                }
            }
        }

        public static bool Comanda(List<string> iteme_comanda)
        {
            Dictionary<string, int> preparate = new Dictionary<string, int>();
            ListeazaPreparateComanda(iteme_comanda, preparate);
            foreach(KeyValuePair<string, int> pereche in preparate)
            {
                if (pereche.Value > CautaCantitatePreparat(pereche.Key))
                    return false;
            }
            UpdateBD(preparate);

            return true; 
        }

        public static void UpdateBD(Dictionary<string, int> preparate)
        {
            LinkToSqlClassDataContext dc = new LinkToSqlClassDataContext(Properties.Settings.Default.RestaurantConnectionString);
            foreach (KeyValuePair<string, int> pereche in preparate)
            {
                Preparat prep = dc.Preparats.SingleOrDefault(x => x.Denumire == pereche.Key);
                prep.CantitateTotala -= Math.Round(Utils.GtoKg(pereche.Value), 2);
                dc.SubmitChanges();
            }
            
        }

        public static int CalculTotal(List<string> iteme_comanda)
        {
            int comanda = 0;
            foreach(string s in iteme_comanda)
            {
                List<string> item = s.Split(',').ToList();
                string pret = item[item.Count()-1].TrimStart(' ');
                pret = pret.Remove(pret.Length - 3, 3);
                comanda += int.Parse(pret);
            }
            return comanda;
        }

        public static void InsertComanda(List<string> iteme_comanda, int pret, int idClient, bool cash)
        {
            LinkToSqlClassDataContext dc = new LinkToSqlClassDataContext(Properties.Settings.Default.RestaurantConnectionString);
            int id = dc.Comandas.ToList().Count +1;
            Random random = new Random();
            int idAngajat = dc.Angajats.ToList().Count();
            Comanda comanda = new Comanda
            {
                IdComanda = id,
                IdStare = 1,
                IdClient = idClient,
                IdAngajat = random.Next(1,idAngajat+1),
                DeAchitat = pret,
                AchitatCash = cash,
                AchitatCard = !cash,
                Data = DateTime.Now
            };

            dc.Comandas.InsertOnSubmit(comanda);
            dc.SubmitChanges();

            JoinComanda(iteme_comanda, id);
        }

        public static void ListeazaComanda(Dictionary<string, int> dict, List<string> iteme_comanda)
        {
            foreach(string s in iteme_comanda)
            {
                List<string> item = s.Split(',').ToList();
                string denumire = item[0].TrimStart(' ');
                if (denumire.Contains("Meniu"))
                {
                    if (dict.ContainsKey(denumire))
                        dict[denumire] += 1;
                    else
                        dict[denumire] = 1; 
                }
                else
                {
                    if (dict.ContainsKey(denumire))
                        dict[denumire] += int.Parse(item[1].TrimEnd('g').TrimStart(' '));
                    else
                        dict[denumire] = int.Parse(item[1].TrimEnd('g').TrimStart(' '));
                }
            }
        }

        public static void JoinComanda(List<string> iteme_comanda, int idComanda)
        {
            LinkToSqlClassDataContext dc = new LinkToSqlClassDataContext(Properties.Settings.Default.RestaurantConnectionString);
            Dictionary<string, int> comenzi = new Dictionary<string, int>();
            ListeazaComanda(comenzi, iteme_comanda);

            foreach (KeyValuePair<string, int> comanda in comenzi)
            {
                if (comanda.Key.Contains("Meniu"))
                {
                    int ultim = dc.ComandaMeniuJoins.ToList().Count + 1;
                    int idMeniu = dc.Menius.SingleOrDefault(x => x.Denumire == comanda.Key).IdMeniu;

                    ComandaMeniuJoin comandaMeniuJoin = new ComandaMeniuJoin
                    {
                        IdCM = ultim,
                        IdComanda = idComanda,
                        IdMeniu = idMeniu,
                        Cantitate = (double)comanda.Value
                    };
                    dc.ComandaMeniuJoins.InsertOnSubmit(comandaMeniuJoin);
                    dc.SubmitChanges();                   
                }
                else
                {
                    int ultim = dc.ComandaPreparatJoins.ToList().Count+1;
                    int idPreparat = dc.Preparats.SingleOrDefault(x => x.Denumire == comanda.Key).IdPreparat;

                    ComandaPreparatJoin comandaPrepJoin = new ComandaPreparatJoin
                    {
                        IdCP = ultim,
                        IdComanda = idComanda,
                        IdPreparat = idPreparat,
                        Cantitate = (double)comanda.Value
                    };
                    dc.ComandaPreparatJoins.InsertOnSubmit(comandaPrepJoin);
                    dc.SubmitChanges();
                }
            }        
            
        }
    }

}
