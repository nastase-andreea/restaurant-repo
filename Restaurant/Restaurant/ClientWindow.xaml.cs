﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFCustomMessageBox;

namespace Restaurant
{
    /// <summary>
    /// Interaction logic for ClientWindow.xaml
    /// </summary>
    public partial class ClientWindow : Window
    {
        public ClientWindow()
        {
            InitializeComponent();
        }
        private void makeInvisiblePreparate()
        {
            Button[] btnList = { btn_bauturi, btn_garnituri, btn_lactate, btn_mixt, btn_paste, btn_peste, btn_porc, btn_pui, btn_salate };

            foreach (Button button in btnList)
            {
                button.Visibility = Visibility.Hidden;
            }
        }
        private void makeVisiblePreparate()
        {
            Button[] btnList = { btn_bauturi, btn_garnituri, btn_lactate, btn_mixt, btn_paste, btn_peste, btn_porc, btn_pui, btn_salate };

            foreach (Button button in btnList)
            {
                button.Visibility = Visibility.Visible;
            }
        }
        private void btn_meniuri_Click(object sender, RoutedEventArgs e)
        {
            makeInvisiblePreparate();
            Button button = (Button)sender;
            List<Meniu> list = Utils.GetMeniuri(button.Content.ToString());
            listItems.ItemsSource = Utils.ArataMeniu(list);
        }

        private void btn_preparate_Click(object sender, RoutedEventArgs e)
        {
            makeVisiblePreparate();
        }

        private void btn_preparat_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            List<Preparat> list = Utils.GetPreparate(button.Content.ToString());
            listItems.ItemsSource = Utils.ArataPreparat(list);
        }
        private void txt_search_Enter(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                listItems.ItemsSource = Utils.Filtreaza(txt_search.Text);
            }
        }
        private void btn_search_Click(object sender, RoutedEventArgs e)
        {
            listItems.ItemsSource = Utils.Filtreaza(txt_search.Text);
        }

        private void btn_cos_Click(object sender, RoutedEventArgs e)
        {
            if(list_cos.Visibility == Visibility.Hidden)
                list_cos.Visibility = Visibility.Visible;
            else
                list_cos.Visibility = Visibility.Hidden;
        }   
        private void btn_adauga_cos_Click(object sender, RoutedEventArgs e)
        {
            if(listItems.SelectedItem != null)
            {
                list_cos.Items.Add(Utils.GetDescription((MenuItem)listItems.SelectedItem));
            }
        }

        private void btn_goleste_cos_Click(object sender, RoutedEventArgs e)
        {
            list_cos.Items.Clear();
        }

        private void btn_finalizare_Click(object sender, RoutedEventArgs e)
        {
            var stringList = list_cos.Items.OfType<string>();
            if (Utils.Comanda(stringList.ToList<string>()))
            {
                int pret = Utils.CalculTotal(stringList.ToList<string>());
                Utils.InsertComanda(stringList.ToList<string>(), pret, int.Parse(id_client.Text),
                    CustomMessageBox.ShowOKCancel("Cu ce preferati sa platiti?", "Plata", "Cash", "Card") == MessageBoxResult.OK);

                MessageBox.Show("Comanda inregistrata cu succes!", "Succes");
                list_cos.Items.Clear();
            }
            else
            {
                MessageBox.Show("Nu avem cantitatea necesara de preparate pentru comanda.", "Comanda invalida");
            }
        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow();
            window.Show();
            Close();
        }
    }
}
