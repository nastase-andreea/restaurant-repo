﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant
{
    public class FindImageUtils
    {
        public static string FindPreparatImage(string nume)
        {
            switch (nume)
            {
                case "Aripioare BBQ":
                    {
                        return "Resources/aripioare_bbq.jpg";
                    }
                case "Aripioare Picante":
                    {
                        return "Resources/aripioare_picante.jpg";
                    }
                case "Carnati de Porc":
                    {
                        return "Resources/carnati_porc.jpg";
                    }
                case "Cascaval Pane":
                    {
                        return "Resources/cascaval_pane.jpg";
                    }
                case "Ceafa de Porc":
                    {
                        return "Resources/ceafa_porc.jpg";
                    }
                case "Somon la Gratar":
                    {
                        return "Resources/somon.jpg";
                    }
                case "Snitel de Pui":
                    {
                        return "Resources/snitel.jpg";
                    }
                case "Pulpa de Pui":
                    {
                        return "Resources/pulpa_pui.jpg";
                    }
                case "File cod Alaska":
                    {
                        return "Resources/alaska.jpg";
                    }
                case "Nuggets":
                    {
                        return "Resources/nuggets.jpg";
                    }
                case "Branza cu Smantana":
                    {
                        return "Resources/branza_smantana_mamaliga.jpg";
                    }
                case "Cartofi prajiti":
                    {
                        return "Resources/cartofi_prajiti.jpg";
                    }
                case "Cartofi piure":
                    {
                        return "Resources/piure.jpg";
                    }
                case "Orez salbatic":
                    {
                        return "Resources/orez.jpg";
                    }
                case "Salata de Vara":
                    {
                        return "Resources/salata_vara.jpg";
                    }
                case "Salata de Varza":
                    {
                        return "Resources/salata_varza.jpg";
                    }
                case "Salata de Sfecta":
                    {
                        return "Resources/salata_sfecla.jpg";
                    }
                case "Salata Cezar":
                    {
                        return "Resources/salata_cezar.jpg";
                    }
                case "Penne Arrabiata":
                    {
                        return "Resources/arrabiata.jpg";
                    }
                case "Spaghete Carbonara":
                    {
                        return "Resources/carbonara.jpg";
                    }
                case "Spaghete Bologneze":
                    {
                        return "Resources/bolognese.jpg";
                    }
                case "Pepsi":
                    {
                        return "Resources/pepsi.jpg";
                    }
                case "Sprite":
                    {
                        return "Resources/sprite.jpg";
                    }
                case "Fanta":
                    {
                        return "Resources/fanta.jpg";
                    }
                case "Ciucas":
                    {
                        return "Resources/ciucas.jpg";
                    }
                case "Mamaliguta":
                    {
                        return "Resources/mamaliga.jpg";
                    }
                case "Tochitura":
                    {
                        return "Resources/tochitura.jpg";
                    }
            }

            return string.Empty;
        }

        public static string FindMeniuImage(string nume)
        {
            switch (nume)
            {
                case "Meniu Aripioare BBQ":
                    {
                        return "Resources/meniu_aripioare_bbq.jpg";
                    }
                case "Meniu Aripioare Picante":
                    {
                        return "Resources/meniu_aripioare_picante.jpg";
                    }
                case "Meniu Carnati de Porc":
                    {
                        return "Resources/meniu_carnati_porc.jpg";
                    }
                case "Meniu Cascaval Pane":
                    {
                        return "Resources/meniu_cascaval_pane.jpg";
                    }
                case "Meniu Ceafa de Porc":
                    {
                        return "Resources/meniu_ceafa_porc.jpg";
                    }
                case "Meniu Somon la Grafar":
                    {
                        return "Resources/meniu_somon.jpg";
                    }
                case "Meniu Snitel de Pui":
                    {
                        return "Resources/meniu_snitel.jpg";
                    }
                case "Maniu Pulpa de Pui":
                    {
                        return "Resources/meniu_pulpa.jpg";
                    }
                case "Meniu File cod Alaska":
                    {
                        return "Resources/meniu_alaska.jpg";
                    }
                case "Meniu Copii":
                    {
                        return "Resources/meniu_nuggets.jpg";
                    }
                case "Meniu Familie( 3 pers.)":
                    {
                        return "Resources/meniu_familie.jpg";
                    }
                case "Meniu Traditional Romanesc":
                    {
                        return "Resources/meniu_tochitura.jpg";
                    }
            }

            return string.Empty;
        }
    }
}
