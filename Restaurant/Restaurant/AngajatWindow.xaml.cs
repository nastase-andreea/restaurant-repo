﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Restaurant
{
    /// <summary>
    /// Interaction logic for AngajatWindow.xaml
    /// </summary>
    public partial class AngajatWindow : Window
    {
        private LinkToSqlClassDataContext dc = new LinkToSqlClassDataContext(Properties.Settings.Default.RestaurantConnectionString);

        public AngajatWindow()
        {
            InitializeComponent();
        }

        private void btn_alergen_Click(object sender, RoutedEventArgs e)
        {
            grid_tabel.ItemsSource = dc.Alergens;
        }

        private void btn_comanda_Click(object sender, RoutedEventArgs e)
        {
            grid_tabel.ItemsSource = dc.Comandas;
        }

        private void btn_meniu_Click(object sender, RoutedEventArgs e)
        {
            grid_tabel.ItemsSource = dc.Menius;
        }

        private void btn_tip_meniu_Click(object sender, RoutedEventArgs e)
        {
            grid_tabel.ItemsSource = dc.MeniuTips;
        }

        private void btn_preparat_Click(object sender, RoutedEventArgs e)
        {
            grid_tabel.ItemsSource = dc.Preparats;
        }

        private void btn_tip_preparat_Click(object sender, RoutedEventArgs e)
        {
            grid_tabel.ItemsSource = dc.PreparatTips;
        }

        private void btn_stare_Click(object sender, RoutedEventArgs e)
        { 
            grid_tabel.ItemsSource = dc.Stares;
        }

        private void btn_comanda_meniu_Click(object sender, RoutedEventArgs e)
        {
            grid_tabel.ItemsSource = dc.ComandaMeniuJoins;
        }

        private void btn_comanda_preparat_Click(object sender, RoutedEventArgs e)
        {
            grid_tabel.ItemsSource = dc.ComandaPreparatJoins;
        }

        private void btn_meniu_preparat_Click(object sender, RoutedEventArgs e)
        {
            grid_tabel.ItemsSource = dc.MeniuPreparatJoins;
        }

        private void btn_preparat_alergen_Click(object sender, RoutedEventArgs e)
        {
            grid_tabel.ItemsSource = dc.PreparatAlergenJoins;
        }

        private void btn_save_change_Click(object sender, RoutedEventArgs e)
        {
            dc.SubmitChanges();
            MessageBox.Show("S-au salvat modificarile!", "Succes");
        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow();
            window.Show();
            Close();
        }
    }
}
