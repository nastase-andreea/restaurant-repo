﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Restaurant
{
    /// <summary>
    /// Interaction logic for CreareContWindow.xaml
    /// </summary>
    public partial class CreareContWindow : Window
    {
        public CreareContWindow()
        {
            InitializeComponent();
        }

        private void btn_submit_Click(object sender, RoutedEventArgs e)
        {
            if (Utils.CampGol(txt_nume.Text, txt_prenume.Text, txt_email.Text, txt_parola.Password, txt_telefon.Text, txt_adresa.Text))
            { 
                MessageBox.Show("Nu ati introdus toate datele necesare!", "Date incomplete"); 
            }
            else if(Utils.ClientExistent(txt_email.Text))
            {
                MessageBox.Show("Email-ul este deja asociat unui cont!", "Email existent");
            }
            else
            {
                Utils.AdaugaClient(txt_nume.Text, txt_prenume.Text, txt_email.Text, txt_parola.Password, txt_telefon.Text, txt_adresa.Text);
                MessageBox.Show("Cont creat cu succes!", "Succes");
                Window MainWindow = new MainWindow();
                MainWindow.Show();
                Close();
            }
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            Window MainWindow = new MainWindow();
            MainWindow.Show();
            Close();
        }
    }
}
