﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Restaurant
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        private void btn_login_Click(object sender, RoutedEventArgs e)
        {
            int tipWindow = Utils.VerifyUser(txt_email.Text, txt_password.Password);
            switch(tipWindow)
            {
                case 0:
                    {
                        MessageBoxResult result = MessageBox.Show("Ati introdus email-ul sau parola gresit!",
                                          "Date incorecte");
                        break;
                    }
                case 1:
                    {
                        ClientWindow window = new ClientWindow();
                        window.id_client.Text = Utils.CautaIdClient(txt_email.Text, txt_password.Password).ToString();
                        window.Show();
                        Close();
                        break;
                    }
                case 2:
                    {
                        AngajatWindow window = new AngajatWindow();
                        window.Show();
                        Close();
                        break;
                    }
            }
        }

        private void btn_new_account_Click(object sender, RoutedEventArgs e)
        {
            CreareContWindow window = new CreareContWindow();
            window.Show();
            Close();
        }

        private void btn_no_account_Click(object sender, RoutedEventArgs e)
        {
            ClientWindow window = new ClientWindow();
            window.btn_adauga_cos.Visibility = Visibility.Hidden;
            window.btn_finalizare.Visibility = Visibility.Hidden;
            window.btn_cos.Visibility = Visibility.Hidden;
            window.btn_goleste_cos.Visibility = Visibility.Hidden;
            window.Show();
            Close();
        }
    }
}
