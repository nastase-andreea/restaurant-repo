﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Restaurant
{
    public class MenuItem
    {

        private string _Description;
        private BitmapImage _ImageData;
        public string Description
        {
            get { return this._Description; }
            set { this._Description = value; }
        }

        public BitmapImage ImageData
        {
            get { return this._ImageData; }
            set { this._ImageData = value; }
        }
   
    }
}
