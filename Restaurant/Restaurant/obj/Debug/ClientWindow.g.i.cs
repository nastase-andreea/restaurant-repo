﻿#pragma checksum "..\..\ClientWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "BAA0E2B1A3DACA1DB392458187C576B9A51876D7E8B26B855C1DFD317D7B390B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Restaurant;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Restaurant {
    
    
    /// <summary>
    /// ClientWindow
    /// </summary>
    public partial class ClientWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 49 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_search;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_search;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_meniuri;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_preparate;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_pui;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_porc;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_peste;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_paste;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_garnituri;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_salate;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_mixt;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_lactate;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_bauturi;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock id_client;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox listItems;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_cos;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox list_cos;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_goleste_cos;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_adauga_cos;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_finalizare;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\ClientWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_back;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Restaurant;component/clientwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ClientWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.txt_search = ((System.Windows.Controls.TextBox)(target));
            
            #line 49 "..\..\ClientWindow.xaml"
            this.txt_search.KeyDown += new System.Windows.Input.KeyEventHandler(this.txt_search_Enter);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btn_search = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\ClientWindow.xaml"
            this.btn_search.Click += new System.Windows.RoutedEventHandler(this.btn_search_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btn_meniuri = ((System.Windows.Controls.Button)(target));
            
            #line 55 "..\..\ClientWindow.xaml"
            this.btn_meniuri.Click += new System.Windows.RoutedEventHandler(this.btn_meniuri_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btn_preparate = ((System.Windows.Controls.Button)(target));
            
            #line 56 "..\..\ClientWindow.xaml"
            this.btn_preparate.Click += new System.Windows.RoutedEventHandler(this.btn_preparate_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btn_pui = ((System.Windows.Controls.Button)(target));
            
            #line 57 "..\..\ClientWindow.xaml"
            this.btn_pui.Click += new System.Windows.RoutedEventHandler(this.btn_preparat_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.btn_porc = ((System.Windows.Controls.Button)(target));
            
            #line 58 "..\..\ClientWindow.xaml"
            this.btn_porc.Click += new System.Windows.RoutedEventHandler(this.btn_preparat_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btn_peste = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\ClientWindow.xaml"
            this.btn_peste.Click += new System.Windows.RoutedEventHandler(this.btn_preparat_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btn_paste = ((System.Windows.Controls.Button)(target));
            
            #line 60 "..\..\ClientWindow.xaml"
            this.btn_paste.Click += new System.Windows.RoutedEventHandler(this.btn_preparat_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.btn_garnituri = ((System.Windows.Controls.Button)(target));
            
            #line 61 "..\..\ClientWindow.xaml"
            this.btn_garnituri.Click += new System.Windows.RoutedEventHandler(this.btn_preparat_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.btn_salate = ((System.Windows.Controls.Button)(target));
            
            #line 62 "..\..\ClientWindow.xaml"
            this.btn_salate.Click += new System.Windows.RoutedEventHandler(this.btn_preparat_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.btn_mixt = ((System.Windows.Controls.Button)(target));
            
            #line 63 "..\..\ClientWindow.xaml"
            this.btn_mixt.Click += new System.Windows.RoutedEventHandler(this.btn_preparat_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.btn_lactate = ((System.Windows.Controls.Button)(target));
            
            #line 64 "..\..\ClientWindow.xaml"
            this.btn_lactate.Click += new System.Windows.RoutedEventHandler(this.btn_preparat_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.btn_bauturi = ((System.Windows.Controls.Button)(target));
            
            #line 65 "..\..\ClientWindow.xaml"
            this.btn_bauturi.Click += new System.Windows.RoutedEventHandler(this.btn_preparat_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.id_client = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 15:
            this.listItems = ((System.Windows.Controls.ListBox)(target));
            return;
            case 16:
            this.btn_cos = ((System.Windows.Controls.Button)(target));
            
            #line 88 "..\..\ClientWindow.xaml"
            this.btn_cos.Click += new System.Windows.RoutedEventHandler(this.btn_cos_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.list_cos = ((System.Windows.Controls.ListBox)(target));
            return;
            case 18:
            this.btn_goleste_cos = ((System.Windows.Controls.Button)(target));
            
            #line 95 "..\..\ClientWindow.xaml"
            this.btn_goleste_cos.Click += new System.Windows.RoutedEventHandler(this.btn_goleste_cos_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.btn_adauga_cos = ((System.Windows.Controls.Button)(target));
            
            #line 96 "..\..\ClientWindow.xaml"
            this.btn_adauga_cos.Click += new System.Windows.RoutedEventHandler(this.btn_adauga_cos_Click);
            
            #line default
            #line hidden
            return;
            case 20:
            this.btn_finalizare = ((System.Windows.Controls.Button)(target));
            
            #line 97 "..\..\ClientWindow.xaml"
            this.btn_finalizare.Click += new System.Windows.RoutedEventHandler(this.btn_finalizare_Click);
            
            #line default
            #line hidden
            return;
            case 21:
            this.btn_back = ((System.Windows.Controls.Button)(target));
            
            #line 98 "..\..\ClientWindow.xaml"
            this.btn_back.Click += new System.Windows.RoutedEventHandler(this.btn_back_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

